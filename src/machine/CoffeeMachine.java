package machine;
import java.util.Scanner;
public class CoffeeMachine {
    private int water;
    private int milk;
    private int coffee;
    private int cups;
    private int money;
    private Scanner scanner = new Scanner(System.in);
    private CoffeeMachine(int water,int milk,int coffee,int cups,int money){
        this.water = water;
        this.milk = milk;
        this.coffee = coffee;
        this.cups = cups;
        this.money=money;
    }
    public static void main(String[] args) {
        boolean f = true;
        CoffeeMachine main = new CoffeeMachine(400,540,120,9,550);
        while(f) {
            String action = main.select_action();
            f = main.work(action,main,f);
        }
    }

    private String select_action(){
        System.out.print("Write action (buy, fill, take, remaining, exit): ");
        String action = scanner.next();
        System.out.print("\n");
        return action;
    }
    private boolean work(String action,CoffeeMachine main,boolean f){
        switch (action) {
            case "buy":
                main.buy(main);
                break;
            case "fill":
                main.fill(main);
                break;
            case "take":
                main.take(main);
                break;
            case "remaining":
                main.remainig(main);
                break;
            case "exit":
                f = false;
                break;
        }
        return f;
    }
    private void buy(CoffeeMachine main){
        String sail;
        System.out.print("What do you want to buy? 1 - espresso, 2 - latte, 3 -  cappuccino, back - to main menu: ");
        sail = scanner.next();
        switch (sail) {
            case "1":
                if (main.water < 250) {
                    System.out.println("Sorry, not enough water!");
                    break;
                } else if (main.coffee < 16) {
                    System.out.println("Sorry, not enough coffee beans!");
                    break;
                } else if (main.cups < 1) {
                    System.out.println("Sorry, not enough disposable cups!");
                    break;
                }
                main.water -= 250;
                main.coffee -= 16;
                main.cups -= 1;
                main.money += 4;
                System.out.println("I have enough resources, making you a coffee!");
                break;
            case "2":
                if (main.water < 350) {
                    System.out.println("Sorry, not enough water!");
                    break;
                } else if (main.milk < 75) {
                    System.out.println("Sorry, not enough milk!");
                    break;
                } else if (main.coffee < 20) {
                    System.out.println("Sorry, not enough coffee beans!");
                    break;
                } else if (main.cups < 1) {
                    System.out.println("Sorry, not enough disposable cups!");
                    break;
                }
                main.water -= 350;
                main.milk -= 75;
                main.coffee -= 20;
                main.cups -= 1;
                main.money += 7;
                System.out.println("I have enough resources, making you a coffee!");
                break;
            case "3":
                if (main.water < 200) {
                    System.out.println("Sorry, not enough water!");
                    break;
                } else if (main.milk < 100) {
                    System.out.println("Sorry, not enough milk!");
                    break;
                } else if (main.coffee < 12) {
                    System.out.println("Sorry, not enough coffee beans!");
                    break;
                } else if (main.cups < 1) {
                    System.out.println("Sorry, not enough disposable cups!");
                    break;
                }
                main.water -= 200;
                main.milk -= 100;
                main.coffee -= 12;
                main.cups -= 1;
                main.money += 6;
                System.out.println("I have enough resources, making you a coffee!");
                break;
            case "back":
                break;
        }
    }
    private void fill(CoffeeMachine main){
        //Scanner scanner= new Scanner(System.in);
        System.out.print("Write how many ml of water do you want to add: ");
        main.water += scanner.nextInt();
        System.out.print("Write how many ml of milk do you want to add: ");
        main.milk += scanner.nextInt();
        System.out.print("Write how many grams of coffee beans do you want to add: ");
        main.coffee += scanner.nextInt();
        System.out.print("Write how many disposable cups of coffee do you want to add: ");
        main.cups += scanner.nextInt();
    }
    private void take(CoffeeMachine main){
        System.out.println("I gave you $" + main.money);
        main.money = 0;
    }
    private void remainig(CoffeeMachine main) {
        System.out.println("The coffee machine has:\n" +
                main.water + " of water\n" +
                main.milk + " of milk\n" +
                main.coffee + " of coffee beans\n" +
                main.cups + " of disposable cups\n" +
                "$"+ main.money + " of money\n");
    }
}